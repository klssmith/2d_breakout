document.addEventListener("DOMContentLoaded", function(event) {

  var canvas = document.getElementById("myCanvas");
  var ctx = canvas.getContext("2d");
  var x = canvas.width/2;
  var y = canvas.height-30;
  var dx = 2;
  var dy = -2;
  var ballRadius = 10;
  var ballColor = "blue";
  var paddleHeight = 5;
  var paddleWidth = 75;
  var paddleX = (canvas.width-paddleWidth)/2;
  var rightPressed = false;
  var leftPressed = false;
  var score = 0;
  var lives = 3;

  // brick variables
  var currentBrick;
  var brickColor = ["blue", "chartreuse", "red"];
  var brickRowCount = 3;
  var brickColumnCount = 5;
  var brickWidth = 75;
  var brickHeight = 20;
  var brickPadding = 10;
  var brickOffsetTop = 40;
  var brickOffsetLeft = 30;
  var bricks = [];
  for(c=0; c<brickColumnCount; c++) {
    bricks[c] = [];
    for(r=0; r<brickRowCount; r++) {
        bricks[c][r] = { x: 0, y: 0, status: 3 };
    }
  }

  document.addEventListener("keydown", keyDownHandler, false);
  document.addEventListener("keyup", keyUpHandler, false);
  document.addEventListener("mousemove", mouseMoveHandler, false);

  function keyDownHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = true;
    } else if(e.keyCode == 37) {
        leftPressed = true;
    }
  }

  function keyUpHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = false;
    } else if(e.keyCode == 37) {
        leftPressed = false;
    }
  }

  function mouseMoveHandler(e) {
    var relativeX = e.clientX - canvas.offsetLeft;
    if(relativeX > 0 && relativeX < canvas.width) {
      paddleX = relativeX - paddleWidth/2;
    }
  }

  function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  function drawLives() {
    ctx.font = "18px Arial";
    ctx.fillStyle = "red";
    ctx.fillText("Lives: " +lives, canvas.width-70, 20);
  }

  function collisionDetection() {
      for(c=0; c<brickColumnCount; c++) {
          for(r=0; r<brickRowCount; r++) {
              currentBrick = bricks[c][r];
              if(currentBrick.status > 0) {
                  if(isBrickHit(currentBrick)) {
                    logBrickHit();
                    checkIfGameWon();
                  }
              }
          }
      }
  }

  function isBrickHit(currentBrick) {
    return (x > currentBrick.x && x < currentBrick.x+brickWidth && y > currentBrick.y && y < currentBrick.y+brickHeight);
  }

  function logBrickHit() {
    dy = -dy;
    currentBrick.status -= 1;
    ballColor = getRandomColor();
    score++;
  }

  function checkIfGameWon() {
    if(score == 3 * brickRowCount * brickColumnCount) {
      alert("YOU WIN, CONGRATULATIONS!");
      document.location.reload();
    }
  }

  function checkIfGameLost() {
    if(!lives) {
      alert("GAME OVER");
      document.location.reload();
    }
  }

  function fillAndCloseCanvas(color) {
    ctx.fillStyle = color;
    ctx.fill();
    ctx.closePath();
  }

  function drawScore() {
    ctx.font = "18px Arial";
    ctx.fillStyle = "red";
    ctx.fillText("Score: "+score, 8, 20);
  }

  function drawBall() {
      ctx.beginPath();
      ctx.arc(x, y, ballRadius, 0, Math.PI*2);
      fillAndCloseCanvas(ballColor);
  }

  function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height-paddleHeight, paddleWidth, paddleHeight);
    fillAndCloseCanvas("red");
  }

  function drawBricks() {
      for(c=0; c<brickColumnCount; c++) {
          for(r=0; r<brickRowCount; r++) {
            currentBrick = bricks[c][r];
            if(currentBrick.status > 0) {
              currentBrick.x = (c*(brickWidth+brickPadding))+brickOffsetLeft;
              currentBrick.y = (r*(brickHeight+brickPadding))+brickOffsetTop;
              ctx.beginPath();
              ctx.rect(currentBrick.x, currentBrick.y, brickWidth, brickHeight);
              fillAndCloseCanvas(brickColor[3 - currentBrick.status]);
            }
          }
      }
  }

  function ballHitsWall() {
    if(x + dx > canvas.width-ballRadius || x + dx < ballRadius) {
      return true;
    }
  }

  function ballHitsTop() {
    if(y + dy < ballRadius) {
      return true;
    }
  }

  function ballHitsBase() {
    if(y + dy > canvas.height-ballRadius) {
      return true;
    }
  }

  function ballHitsPaddle() {
    if(x > paddleX && x < paddleX + paddleWidth) {
      return true;
    }
  }

  function draw() {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      drawBricks();
      drawBall();
      drawPaddle();
      collisionDetection();
      drawScore();
      drawLives();

      if(ballHitsWall()) {
          dx = -dx;
          ballColor = getRandomColor();
      }
      if(ballHitsTop()) {
        dy = -dy;
      } else if(ballHitsBase()) {
        if(ballHitsPaddle()) {
          dy = -dy;
        } else {
          lives--;
          checkIfGameLost();
          x = canvas.width/2;
          y = canvas.height-30;
          dx = 2;
          dy = -2;
          paddleX = (canvas.width-paddleWidth)/2;
        }
      }

      if(rightPressed && paddleX < canvas.width-(paddleWidth)) {
          paddleX += 7;
      }
      else if(leftPressed && paddleX > 0) {
          paddleX -= 7;
      }

      x += dx;
      y += dy;
    requestAnimationFrame(draw);
  }

  draw();
});
