#2D BreakOut

##About the Game
This is a simple game which is adapted and developed from the tutorial found here https://developer.mozilla.org/en-US/docs/Games/Workflows/2D_Breakout_game_pure_JavaScript. While the tutorial was followed initially in order to create it, there have been many changes made to the code, styling and gameplay.

##How to Play
* Clone this respository to your computer in order to get the files.
* No installation is needed, simply open the index.html file in your browser and the game will start automatically.
